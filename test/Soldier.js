var assert = require('chai').assert
var expect = require('chai').expect

var Soldier = require('../src/Models/Soldier')



describe('Soldier', function() {
    it('damaged()', function() {

      var soldier1 = new Soldier(200, 100, 'soldier1')

      soldier1.damaged(10);

      assert.equal(soldier1.health, 90, 'the soldier1 shoud have 90 healts');
    });
    it('addExperience()', function () {
      var soldier2 = new Soldier(300,'soldier2')
      soldier2.addExperience();
      assert.equal(soldier2.experience,1);
      soldier2.experience = 50;
      soldier2.addExperience()
      assert.equal(soldier2.experience, 50)
    })
    it('constructor', function () {

      var soldier3 = new Soldier(200, 100, 'soldier3');

      assert.equal(soldier3.health, 100)
      assert.equal(soldier3.experience, 0)
      assert.equal(soldier3.recharge, 200)
      assert.equal(soldier3.name, 'soldier soldier3')

    })
  });
