var assert = require('chai').assert;
var expect = require('chai').expect;
var Soldier = require('../src/Models/Soldier');
var Vehicle = require('../src/Models/Vehicle');

describe('Vehicle', function() {
    it('constructor', function () {
      var operators = []
      for(var i = 0; i < 3; i++){
        operators.push(new Soldier(200,100,i))
      }
      var vehicle = new Vehicle(operators,1500, 100, 'first')

      assert.equal(vehicle.health, 200);
      assert.equal(vehicle.recharge, 1500);
      assert.equal(vehicle.name, 'vehicle first');
      assert.equal(vehicle.operators.length, 3);

      for(var i = 0; i < vehicle.operators.length; i++){
        assert(vehicle.operators[i].name, ' vehicle first soldier ')
      }

    })
    it('damaged() with 3 operators', function() {
      var operators = []
      for(var i = 0; i < 3; i++){
        operators.push(new Soldier(200,100,i))
      }
      var vehicle = new Vehicle(operators,1300,100,'1')

      vehicle.damaged(10);
      assert.equal(vehicle.health, 197)
      var mostDamage = 99999;
      var operatorWithMostDamage = '';
      for(var i = 0; i < vehicle.operators.length; i++){
        if(vehicle.operators[i].health < mostDamage){
          mostDamage = vehicle.operators[i].health;
          operatorWithMostDamage = i;
        }
      }

      assert.equal(vehicle.operators[operatorWithMostDamage].health, 95)
      for(var i = 0; i < vehicle.operators.length; i++){
        if(i !== operatorWithMostDamage){
          assert.equal(vehicle.operators[i].health,99)
        }
      }
    });
    it('damaged() with 2 operators', function (){
      var operators = []
      for(var i = 0; i < 2; i++){
        operators.push(new Soldier(200,100,i))
      }
      var vehicle = new Vehicle(operators,1300,100,'2')
      vehicle.damaged(10);
      assert.equal(vehicle.health, 197)
      var mostDamage = 99999;
      var operatorWithMostDamage = '';
      for(var i = 0; i < vehicle.operators.length; i++){
        if(vehicle.operators[i].health < mostDamage){
          mostDamage = vehicle.operators[i].health;
          operatorWithMostDamage = i;
        }
      }

      assert.equal(vehicle.operators[operatorWithMostDamage].health, 95)
      for(var i = 0; i < vehicle.operators.length; i++){
        if(i !== operatorWithMostDamage){
          assert.equal(vehicle.operators[i].health,98)
        }
      }
    })

    it('damaged() with 1 operator', function () {
      var operators = []
      for(var i = 0; i < 1; i++){
        operators.push(new Soldier(200,100,i))
      }
      var vehicle = new Vehicle(operators,1300,100,'2')
      vehicle.damaged(10);
      assert.equal(vehicle.health, 195)
      assert.equal(vehicle.operators[0].health, 95)
    })
  });
