var assert = require('chai').assert;
var expect = require('chai').expect;
var Soldier = require('../src/Models/Soldier');
var Vehicle = require('../src/Models/Vehicle');
var Squad = require('../src/Models/Squad');

describe('Squad', function() {
    it('constructor', function () {
      var units = []
      for(var i = 0; i < 10; i++){
        units.push(new Soldier(300,100,i));
      }
      var squad = new Squad(units,0,'1');

      assert.equal(squad.units.length, 10);
      assert.equal(squad.strategy, 0);
      assert.equal(squad.name, 'squad 1');

      for(var i = 0; i < squad.units.length; i++){
        assert(squad.units[i].name, ' squad 1' + i);
      }
    })
    it('damaged()', function() {
        var units = []
        for(var i = 0; i < 10; i++){
          units.push(new Soldier(300,100,i));
        }
        var squad = new Squad(units,0,'1');

        squad.damaged(10);

        for(var i = 0; i < squad.units.length; i++){
          assert(squad.units[i].health, 99);
        }

    });
    it('calculateTotalHealth()', function () {
      var units = []
      for(var i = 0; i < 10; i++){
        units.push(new Soldier(300,100,i));
      }
      var squad = new Squad(units,0,'1');

      assert.equal(squad.getHealth(),1000);

    })
    it('calculateTotalDamage()', function () {
      var units = []
      for(var i = 0; i < 10; i++){
        units.push(new Soldier(300,100,i));
      }
      var squad = new Squad(units,0,'1');
      var totalSquadDamage = Math.round(squad.calculateDamage() * 10 ) / 10;
      assert.equal(totalSquadDamage,0.5);
    })
    it('calculageTotalExperience()', function () {
      var units = []
      for(var i = 0; i < 10; i++){
        var soldier = new Soldier(300,100,i);
        soldier.experience = 5;
        units.push(soldier);
      }
      var squad = new Squad(units,0,'1');
      assert.equal(squad.calculateExperience(), 5);
    })
  });
