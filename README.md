#**Battle simulator**

This is a test used for the assessment of the node.js back-end developer position.

#**Download**

```
cd /path_to_file_where_you_want_to_clone
```
```
git clone https://makevicdusan@bitbucket.org/makevicdusan/battle_simulator.git
```

#**Installation**

```
cd battle_simulator
```
```
npm install
```

#**Run application**

```
npm start
```

#**Run test**

```
npm test
```
