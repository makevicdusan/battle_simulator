/**
* Represents the strategy that the army`s squads uses to select an opponent.
* @const {object} strategies
*/
const strategies = {
  RANDOM: 0,
  WEAKEST: 1,
  STRONGEST: 2,
};
module.exports = strategies;
