/* eslint max-len: ["error", { "ignoreComments": true }] */
/**
* @class This is a class with auxiliary functions.
*/
class Utils {
  /**
  * This is a static function that returns a random number between the minimum and the maximum inclusively.
  * @param {numer} min
  * @param {numer} max
  * @return {number}
  */
  static random(min, max) {
    const min1 = Math.ceil(min);
    const max1 = Math.floor(max);
    return Math.floor(Math.random() * ((max1 - min1) + 1)) + min1;
  }
  /**
  * This is a static function that returns a random array member value.
  * @param {[]} arr
  * @return {number}
  */
  static randomFrom(arr) {
    const min = 0;
    const max = arr.length - 1;

    const rand = this.random(min, max);

    return arr[rand];
  }
  /**
  * This function calculates the geometric average of the attack success of all array members.
  * @param {Soldier[] | Squad[]} arr This can be an array of soldiers or squads.
  * @return {number}
  */
  static gavg(arr) {
    if (arr.length === 0) {
      return 0;
    }
    let gm = 1.0;
    for (let i = 0; i < arr.length; i += 1) {
      gm *= arr[i].calculateAttackProbability();
    }
    gm = Math.pow(gm, 1.0 / arr.length); // eslint-disable-line no-restricted-properties
    return gm;
  }
  /**
  * Displays content in the node console.
  * @param {string} content Content that should be displayed.
  */
  static log(content) {
    console.log(content); // eslint-disable-line no-console
    console.log('* * * *'); // eslint-disable-line no-console
  }
}
module.exports = Utils;
