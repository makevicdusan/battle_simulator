const utils = require('../Utils/Utils');
const Strategy = require('../Strategies/Strategies');
/* eslint max-len: ["error", { "ignoreComments": true }] */
class Battle {
  /**
 * Creates an instance Battle.
 * @this {Battle}
 * @param {Army[]} armies Represents the array of armies in the battle.
 */
  constructor(armies) {
    this.armies = armies;
  }
  /**
  * This function starts the battle.
  * @return {void}
  */
  start() {
    for (let i = 0; i < this.armies.length; i += 1) {
      for (let j = 0; j < this.armies[i].squads.length; j += 1) {
        this.toBattle(this.armies[i].squads[j], this.armies[i]);
      }
    }
  }
  /**
  * The function passes through a number of armies and squads
  * and launches an asynchronous recursive function for each squad.
  * @param {Squad} me The attacking squad.
  * @param {Army} myArmy The attacking army.
  * @return {void}
  */
  toBattle(me, myArmy) {
    try {
      let armyToAttack;
      let squadToAttack;
      let toAttack;
      if (me.strategy === Strategy.RANDOM) {
        toAttack = this.findRandom(myArmy);
        ({ armyToAttack } = toAttack);
        ({ squadToAttack } = toAttack);
      } else if (me.strategy === Strategy.WEAKEST) {
        toAttack = this.findWeakest(myArmy);
        ({ armyToAttack } = toAttack);
        ({ squadToAttack } = toAttack);
      } else if (me.strategy === Strategy.STRONGEST) {
        toAttack = this.findStrongest(myArmy);
        ({ armyToAttack } = toAttack);
        ({ squadToAttack } = toAttack);
      }
      if (me.isAlive() && squadToAttack.isAlive()) {
        Battle.resolveAttack(me, squadToAttack);
        armyToAttack.removeSquads();
        this.removeArmies();
        if (this.armies.length < 2) {
          utils.log(`${me.name} has destroyed the last army!`);
          utils.log(`${myArmy.name} won the battle!!! ------------------- !!!`);
        } else {
          setTimeout(() => {
            this.toBattle(me, myArmy);
          }, me.calculateRecharge());
        }
      }
    } catch (e) { // eslint-disable-line no-empty
      if (me !== undefined) {
        setTimeout(() => {
          this.toBattle(me, myArmy);
        }, me.calculateRecharge());
      }
    }
  }
  /**
  * Finds the strongest squad from all armies except the squads of the attacking army.
  * @param {Army} myArmy The attacking army.
  * @return {{Army,Squad}} Returns the strongest squad and the army he belongs to.
  */
  findStrongest(myArmy) {
    let totalSquadStrength = Number.MIN_SAFE_INTEGER;
    let armyToAttack;
    let squadToAttack;
    for (let i = 0; i < this.armies.length; i += 1) {
      if (myArmy !== this.armies[i]) {
        for (let j = 0; j < this.armies[i].squads.length; j += 1) {
          if (this.armies[i].squads[j].totalStrength() > totalSquadStrength) {
            totalSquadStrength = this.armies[i].squads[j].totalStrength();
            armyToAttack = this.armies[i];
            squadToAttack = this.armies[i].squads[j];
          }
        }
      }
    }
    return { armyToAttack, squadToAttack };
  }
  /**
  * Finds the weakest squad from all armies except the squads of the attacking army.
  * @param {Army} myArmy The attacking army.
  * @return {{Army,Squad}} Returns the weakest squad and the army he belongs to.
  */
  findWeakest(myArmy) {
    let totalSquadStrength = Number.MAX_SAFE_INTEGER;
    let armyToAttack;
    let squadToAttack;
    for (let i = 0; i < this.armies.length; i += 1) {
      if (myArmy !== this.armies[i]) {
        for (let j = 0; j < this.armies[i].squads.length; j += 1) {
          if (this.armies[i].squads[j].totalStrength() < totalSquadStrength) {
            totalSquadStrength = this.armies[i].squads[j].totalStrength();
            armyToAttack = this.armies[i];
            squadToAttack = this.armies[i].squads[j];
          }
        }
      }
    }
    return { armyToAttack, squadToAttack };
  }
  /**
  * Finds random squad from all armies except the squads of the attacking army.
  * @param {Army} myArmy The attacking army.
  * @return {{Army,Squad}} Returns the random squad and the army he belongs to.
  */
  findRandom(myArmy) {
    const possibleArmies = [];
    const possibleSquads = [];
    for (let i = 0; i < this.armies.length; i += 1) {
      if (myArmy !== this.armies[i]) {
        possibleArmies.push(this.armies[i]);
        for (let j = 0; j < this.armies[i].squads.length; j += 1) {
          possibleSquads.push(this.armies[i].squads[j]);
        }
      }
    }
    const a = utils.randomFrom(possibleArmies);
    const s = utils.randomFrom(possibleSquads);
    return { armyToAttack: a, squadToAttack: s };
  }
  /**
  * Function checks if some of the armies have been defeated.
  * If there is not at least one living squad in the army, removes that army from the battle.
  * @return {void}
  */
  removeArmies() {
    for (let i = 0; i < this.armies.length; i += 1) {
      if (!this.armies[i].isAlive()) {
        utils.log(`${this.armies[i].name} is death!!!`);
        this.armies.splice(i, 1);
        i -= 1;
      }
    }
  }
  /**
  * Function calculates which squad has higher attack probability.
  * If it is the attacking squad, damage is dealt to the defending squad.
  * Otherwise no damage is dealt to either side.
  * @param {Squad} attSquad Attacking squad.
  * @param {Squad} defSquad Defending squad.
  * @return {void}
  */
  static resolveAttack(attSquad, defSquad) {
    if (attSquad.calculateAttackProbability() >=
     defSquad.calculateAttackProbability()) {
      defSquad.damaged(attSquad.attack());
    }
  }
}
module.exports = Battle;
