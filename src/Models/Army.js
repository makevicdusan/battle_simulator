const utils = require('../Utils/Utils');
const Vehicle = require('./Vehicle');

class Army {
  /**
 * Creates an instance Army.
 * @this {Army}
 * @param {Squad[]} squads Represents the array of squads in the army.
 * @param {enum} strategy Represents the strategy that the army`s squads use to select an opponent.
 * @param {string} name Represents the name of the army.
 */
  constructor(squads, strategy, name) {
    this.squads = squads;
    this.addStrategy(strategy);
    this.addName(name);
  }
  /**
  * Adds the name of the army to the names of units in it.
  * @return {void}
  */
  addName(name) {
    this.name = `army ${name}`;
    for (let i = 0; i < this.squads.length; i += 1) {
      this.squads[i].name = `${this.name} ${this.squads[i].name}`;
      for (let j = 0; j < this.squads[i].units.length; j += 1) {
        if (this.squads[i].units[j] instanceof Vehicle) {
          this.squads[i].units[j].name = `${this.name} ${this.squads[i].units[j].name}`;
          for (let x = 0; x < this.squads[i].units[j].operators.length; x += 1) {
            this.squads[i].units[j].operators[x].name = `${this.name} ${this.squads[i].units[j].operators[x].name}`;
          }
        } else {
          this.squads[i].units[j].name = `${this.name} ${this.squads[i].units[j].name}`;
        }
      }
    }
  }
  /**
  * Adds strategy to the squads.
  * @return {void}
  */
  addStrategy(strategy) {
    for (let i = 0; i < this.squads.length; i += 1) {
      this.squads[i].strategy = strategy;
    }
  }
  /**
  * Check if any squad is dead.
  * If it is, removes it from the squads array.
  * @return {void}
  */
  removeSquads() {
    for (let i = 0; i < this.squads.length; i += 1) {
      if (this.squads[i].units.length < 1) {
        utils.log(`${this.name} lost the ${this.squads[i].name}!!!`);
        this.squads.splice(i, 1);
        i -= 1;
      }
    }
  }
  /**
  * Checks if the army is alive.
  * @return {boolean}
  */
  isAlive() {
    if (this.squads.length < 1) {
      return false;
    }
    return true;
  }
}
module.exports = Army;
