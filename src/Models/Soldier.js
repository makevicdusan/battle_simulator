const Unit = require('./Unit');
/* eslint max-len: ["error", { "ignoreComments": true }] */
class Soldier extends Unit {
  /**
  * Creates an instance Soldier.
  * @this {Soldier}
  * @extends {Unit}
  * @param {number} recharge Represents the number of ms required to recharge the soldier for an attack.
  * @param {number} health Represents the health of the soldier.
  * @param {string} name Represents the name of the soldier.
  */
  constructor(recharge, health, name) {
    const name1 = `soldier ${name}`;
    super(recharge, health, name1);
    this.experience = 0;
  }
  /**
  * Function represents soldier`s attack. The function also calls the parent function "attack".
  * When the attack is executed successfully adds experience to the soldier.
  * @override
  * @return {number} The amount of damage if attack is successful, otherwise return 0.
  */
  attack() {
    const attack = super.attack();
    if (attack !== 0) {
      this.addExperience();
      return attack;
    }
    return 0;
  }
  /**
  * Adds experience to the unit. The maximum experience is 50.
  * @return {void}
  */
  addExperience() {
    if (this.experience < 50) {
      this.experience += 1;
    }
  }
}
module.exports = Soldier;
