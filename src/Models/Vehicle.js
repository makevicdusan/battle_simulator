const utils = require('../Utils/Utils');
const Unit = require('./Unit');
/* eslint max-len: ["error", { "ignoreComments": true }] */
class Vehicle extends Unit {
  /**
  * Creates an instance Vehicle.
  * At the beginning, health vehicle is calculated.
  * The total health of a vehicle unit is represented
  * as the average health of all its operators and the
  * health of the vehicle.
  * @this {Vehicle}
  * @extends {Unit}
  * @param {Soldier[]} operators The array contains operators which are Soldier type.
  * @param {number} recharge Represents the number of ms required to recharge the vehicle for an attack.
  * @param {number} health Represents the health of the vehicle.
  * @param {string} name Represents the name of the vehicle.
  */
  constructor(operators, recharge, health, name) {
    let health1 = health;
    let opsHealth = 0;
    for (let i = 0; i < operators.length; i += 1) {
      opsHealth += operators[i].getHealth();
    }
    opsHealth /= operators.length;
    health1 += opsHealth;
    const name1 = `vehicle ${name}`;
    super(recharge, health1, name1);
    this.operators = operators;
    this.addName(name);
  }

  /**
  * Adds the name of the vehicle to the names of the operators.
  * @return {void}
  */
  addName() {
    for (let i = 0; i < this.operators.length; i += 1) {
      this.operators[i].name = `${this.name} ${this.operators[i].name}`;
    }
  }

  /**
  * The function is called when the other unit strikes.
  * Parent`s function "damage" is called to damage vehicle.
  * The total damage inflicted on the vehicle is distributed to the operators as follows: 30% of the total
  * damage is inflicted on the vehicle, 50% of the total damage is inflicted on a single random vehicle
  * operator.
  * The rest of the damage is inflicted evenly to the other operators. If there are no additional vehicle
  * operators, the rest of the damage is applied to the vehicle.
  * @override
  * @param {number} damage This is the damage the unit receives.
  * @return {void}
  */
  damaged(damage) {
    if (damage !== 0) {
      let randOp = '';
      const len = this.operators.length - 1;
      if (len > 0) {
        randOp = utils.random(0, len);
      } else {
        randOp = 0;
      }
      this.operators[randOp].damaged(0.5 * damage);
      if (this.operators.length > 1) {
        super.damaged(0.3 * damage);
        for (let i = 0; i <= len; i += 1) {
          if (randOp !== i) {
            if (len === 1) {
              this.operators[i].damaged(0.2 * damage);
            } else {
              this.operators[i].damaged(0.1 * damage);
            }
          }
        }
      } else {
        super.damaged(0.5 * damage);
      }
      if (this.health < 0) {
        this.health = 0;
      }
      this.removeUnit();
    }
  }
  /**
  * Function represents vehicle attack. The function also calls the parent`s function "attack".
  * When the attack is executed successfully adds experience to the operators.
  * @override
  * @return {number} The amount of damage if attack is successful, otherwise return 0.
  */
  attack() {
    const attack = super.attack();
    if (attack !== 0) {
      this.addExperience();
      return attack;
    }
    return 0;
  }
  /**
  * Adds experience to the vehicle operators.
  * @override
  * @return {void}
  */
  addExperience() {
    for (let i = 0; i < this.operators.length; i += 1) {
      this.operators[i].addExperience();
    }
  }
  /**
  * The function calculates the attack probability of the vehicle.
  * @override
  * @return {number} This is the probability that the attack will be successful.
  */
  calculateAttackProbability() {
    return 0.5 * (1 + (this.health / 100)) * utils.gavg(this.operators);
  }
  /**
  * The function calculates the damage that the vehicle afflictes.
  * @override
  * @return {number} The amount of damage.
  */
  calculateDamage() {
    let sumOfExperience = 0;
    for (let i = 0; i < this.operators.length; i += 1) {
      sumOfExperience += this.operators[i].experience;
    }
    if (sumOfExperience === 0) {
      return 0.1;
    }
    return 0.1 + (sumOfExperience / 100);
  }
  /**
  * Check if any operator is dead.
  * If it is, removes it from the "operators" array.
  * @return {void}
  */
  removeUnit() {
    for (let i = 0; i < this.operators.length; i += 1) {
      if (!this.operators[i].isAlive()) {
        utils.log(`Operator ${this.operators[i].name} is dead!`);
        this.operators.splice(i, 1);
        i -= 1;
      }
    }
  }
  /**
  * @override
  * @return {boolean} If the vehicle is alive, it returns true, otherwise false.
  */
  isAlive() {
    if (this.operators.length < 1 || this.health <= 0) {
      return false;
    }
    return true;
  }
  /**
  * @override
  * @return {number} Amount of the vehicle health.
  */
  getHealth() {
    const len = this.operators.length;
    let sumOfHealth = 0;
    for (let i = 0; i < len; i += 1) {
      sumOfHealth += this.operators[i].getHealth();
    }
    return this.health + (sumOfHealth / len);
  }
}
module.exports = Vehicle;
