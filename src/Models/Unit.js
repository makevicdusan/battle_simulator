const utils = require('../Utils/Utils');

class Unit {
  /**
 * Creates an instance Unit.
 * @this {Unit}
 * @param {number} recharge Represents the number of ms required to recharge the unit for an attack.
 * @param {number} health Represents the health of the unit.
 * @param {string} name Represents the name of the unit.
 */
  constructor(recharge, health, name) {
    this.health = health;
    this.recharge = recharge;
    this.name = name;
  }
  /**
  * The function is called when the other unit strikes.
  * @param {number} damage This is the damage the unit receives.
  * @return {void}
  */
  damaged(damage) {
    if (damage !== 0) {
      this.health -= damage;
      if (this.health < 0) {
        this.health = 0;
      }
      utils.log(`${this.name} is damaged by ${damage} and he has ${this.health} healths now!`);
    }
  }
  /**
  * Function represents unit attack.
  * @return {number} The amount of damage if attack is successful, otherwise returns 0.
  */
  attack() {
    const min = 0;
    const prob = this.calculateAttackProbability() * 10;
    const max = 100 * 10;
    const rand = utils.random(min, max);
    if (rand <= prob) {
      utils.log(`${this.name} successfuly attacked!!!!!!!!!!!!!!!`);
      return this.calculateDamage();
    }
    utils.log(`${this.name} missed!!!`);
    return 0;
  }
  /**
  * The function calculates the attack probability of the unit.
  * @return {number}
  */
  calculateAttackProbability() {
    const health1 = this.health / 100;
    const experience1 = 30 + this.experience;
    return (0.5 * (1 + health1)) * (utils.random(experience1, 100) / 100);
  }
  /**
  * The function calculates unit`s damage.
  * @return {number} Returns the amount of damage the unit makes.
  */
  calculateDamage() {
    if (this.experience === 0) {
      return 0.05;
    }
    return 0.05 + (this.experience / 100);
  }
  /**
  * Returns unit`s health.
  * @return {number}
  */
  getHealth() {
    return this.health;
  }
  /**
  * @return {boolean} If the unit is alive, it returns true, otherwise false.
  */
  isAlive() {
    if (this.health <= 0) {
      return false;
    }
    return true;
  }
}
module.exports = Unit;
