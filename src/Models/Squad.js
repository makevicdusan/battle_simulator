const utils = require('../Utils/Utils');
const Soldier = require('./Soldier');
const Vehicle = require('./Vehicle');
const Unit = require('./Unit');
/* eslint max-len: ["error", { "ignoreComments": true }] */
class Squad extends Unit {
  /**
 * Creates an instance Squad.
 * At the beginning, the total squad`s health and the time it takes to recharge are calculated.
 * @this {Squad}
 * @extends {Unit}
 * @param {Soldier[] | Vehicle[]} units Represents the array of units which can be Soldier or Vehicle type.
 * @param {enum} strategy Represents the strategy that the squad uses to select an opponent.
 * @param {string} name Represents the name of the squad.
 */
  constructor(units, strategy, name) {
    let sumOfHealth = 0;
    for (let i = 0; i < units.length; i += 1) {
      sumOfHealth += units[i].getHealth();
    }
    let recharge1 = 0;
    for (let i = 0; i < units.length; i += 1) {
      if (units[i].recharge > recharge1) {
        recharge1 = units[i].recharge;
      }
    }
    const name1 = `squad ${name}`;
    super(recharge1, sumOfHealth, name1);
    this.units = units;
    this.strategy = strategy;
    this.addName(name);
  }
  /**
  * Calculates time takes to recharge squad.
  * @return {number}
  */
  calculateRecharge() {
    let recharge1 = 0;
    for (let i = 0; i < this.units.length; i += 1) {
      if (this.units[i].recharge > recharge1) {
        recharge1 = this.units[i].recharge;
      }
    }
    return recharge1;
  }
  /**
  * Adds the name of the squad to the names of his units.
  * @return {void}
  */
  addName() {
    for (let i = 0; i < this.units.length; i += 1) {
      if (this.units[i] instanceof Vehicle) {
        this.units[i].name = `${this.name} ${this.units[i].name}`;
        for (let j = 0; j < this.units[i].operators.length; j += 1) {
          this.units[i].operators[j].name =
          `${this.name} ${this.units[i].operators[j].name}`;
        }
      } else {
        this.units[i].name = `${this.name} ${this.units[i].name}`;
      }
    }
  }
  /**
  * The function is called when the other squad strikes.
  * At the end, the "removeUnit" function is called to remove dead units from the array "units".
  * @param {number} damage This is the damage the squad receives.
  * @return {void}
  */
  damaged(damage) {
    if (damage === 0) {
      utils.log(`Squad ${this.name} sais: You missed!!!!!!!!`);
    }
    if (damage !== 0) {
      utils.log(`Squad ${this.name} is attacked with ${damage} damage!!!!!!!!`);
      const damagePerUnit = damage / this.units.length;
      for (let i = 0; i < this.units.length; i += 1) {
        this.units[i].damaged(damagePerUnit);
      }
      this.removeUnit();
    }
  }
  /**
  * The function calculates the attack probability of the squad.
  * @return {number} This is the probability that the attack will be successful.
  */
  calculateAttackProbability() {
    const atProb = utils.gavg(this.units);
    return atProb;
  }
  /**
  * Check if any unit is dead.
  * If it is, removes it from the units array.
  * @return {void}
  */
  removeUnit() {
    for (let i = 0; i < this.units.length; i += 1) {
      if (this.units[i] instanceof Soldier && !this.units[i].isAlive()) {
        utils.log(`Soldier ${this.units[i].name} is dead!`);
        this.units.splice(i, 1);
        i -= 1;
      } else if (this.units[i] instanceof Vehicle && !this.units[i].isAlive()) {
        utils.log(`Vehicle ${this.units[i].name} is dead!`);
        this.units.splice(i, 1);
        i -= 1;
      }
    }
  }
  /**
  * @override
  * @return {boolean} If the squad is alive, it returns true, otherwise false.
  */
  isAlive() {
    if (this.units.length < 1) {
      return false;
    }
    return true;
  }
  /**
  * Calculates total strength of the squad.
  * Total strength is a sum of total squad`s healht, experience per unit, number of units and total squad`s damage.
  * @return {number} This is total strength.
  */
  totalStrength() {
    let totalStength = 0;
    totalStength += this.getHealth();
    totalStength += this.calculateExperience();
    totalStength += this.calculateNumOfUnits();
    totalStength += this.calculateDamage();
    return totalStength;
  }
  /**
  * Returns squad`s health.
  * @override
  * @return {number}  Amount of the squad`s health.
  */
  getHealth() {
    let sumOfHealth = 0;
    for (let i = 0; i < this.units.length; i += 1) {
      sumOfHealth += this.units[i].getHealth();
    }
    return sumOfHealth;
  }
  /**
  * Calculates experience per unit.
  * @return {number}  Amount of the squad`s experience.
  */
  calculateExperience() {
    let sumOfExperience = 0;
    for (let i = 0; i < this.units.length; i += 1) {
      if (this.units[i] instanceof Soldier) {
        sumOfExperience += this.units[i].experience;
      } else if (this.units[i] instanceof Vehicle) {
        for (let j = 0; j < this.units[i].operators.length; j += 1) {
          sumOfExperience += this.units[i].operators[j].experience;
        }
      }
    }
    const perUnit = sumOfExperience / this.units.length;
    return perUnit;
  }
  /**
  * Number of units in the squad.
  * @return {number}
  */
  calculateNumOfUnits() {
    return this.units.length;
  }
  /**
  * The function calculates the damage that the squad inflicts.
  * @override
  * @return {number} Amount of damage.
  */
  calculateDamage() {
    let sumOfDamage = 0;
    for (let i = 0; i < this.units.length; i += 1) {
      sumOfDamage += this.units[i].calculateDamage();
    }
    return sumOfDamage;
  }
  /**
  * Adds experience to all units in squad.
  * @return {void}
  */
  addExperience() {
    for (let i = 0; i < this.units.length; i += 1) {
      this.units[i].addExperience();
    }
  }
  /**
  * Function represents squad attack.
  * @override
  * @return {number} The amount of damage if attack is successful, otherwise returns 0.
  */
  attack() {
    const attack = super.attack();
    if (attack > 0) {
      this.addExperience();
      return attack;
    }
    return 0;
  }
}
module.exports = Squad;
