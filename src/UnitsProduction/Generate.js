const Soldier = require('../Models/Soldier');
const Vehicle = require('../Models/Vehicle');
const Squad = require('../Models/Squad');
const Army = require('../Models/Army');
const utils = require('../Utils/Utils');
/* eslint max-len: ["error", { "ignoreComments": true }] */
/**
* This is an auxiliary object that generates armies, squads, vehicles and soldiers.
* @const {object} generate
*/
const generate = {
  /**
  * Generates armies with the parameters that are in the "options" array.
  * @param {object[]} options The length of the array determines the number of armies.
  * @param {number[]} options[i].squads The length of the array determines the number of squads.
  * The number in an array is the number of units in that squad.
  * @param {enum} options[i].strategy Represents the strategy that the army`s squads use to select an opponent.
  * @param {number} options[i].rechargeSoldier Represents the number of ms required to recharge the soldier for an attack.
  * @param {number} options[i].rechargeVehicle Represents the number of ms required to recharge the vehicle for an attack.
  * @return {Army[]}
  */
  generateArmies: (options) => {
    const armies = [];
    for (let i = 0; i < options.length; i += 1) {
      const squads = [];
      for (let j = 0; j < options[i].squads.length; j += 1) {
        squads.push(generate.generateSquad(options[i], j, options[i].squads[j])); // eslint-disable-line
      }
      armies.push(new Army(squads, options[i].strategy, i));
    }
    return armies;
  },
  /**
  * Generates squad with the parameters that are in the "option" object, with "name" and "numberOfUnits" parameters.
  * @param {object} option Contains parameters necessary to create a squad.
  * @param {number} option.rechargeSoldier Represents the number of ms required to recharge the soldier for an attack.
  * @param {number} option.rechargeVehicle Represents the number of ms required to recharge the vehicle for an attack.
  * @param {string} name Represents the name of the squad
  * that is generated as the serial number when the loop goes through an array.
  * @param {number} numOfUnits Represents how many units will be in the squad.
  * @return {Squad}
  */
  generateSquad: (option, name, numOfUnits) => {
    let vehicleOrSoldier = '';
    const units = [];
    for (let i = 0; i < numOfUnits; i += 1) {
      vehicleOrSoldier = utils.random(0, 5);
      if (vehicleOrSoldier < 2) {
        units.push(generate.generateVehicle(option.rechargeVehicle, i));
      } else {
        units.push(generate.generateSoldier(option.rechargeSoldier, i));
      }
    }
    return new Squad(units, option.strategy, name);
  },
  /**
  * Generates vehicle.
  * @param {number} rechargeVehicle Represents the number of ms required to recharge the vehicle for an attack.
  * @param {string} name Represents the name of the vehicle
  * that is generated as the serial number when the loop goes through an array.
  * @return {Vehicle}
  */
  generateVehicle: (rechargeVehicle, name) => {
    const operators = [];
    for (let i = 0; i < 3; i += 1) {
      operators.push(generate.generateSoldier(100, i));
    }
    return new Vehicle(operators, rechargeVehicle, 100, name);
  },
  /**
  * Generates soldier.
  * @param {number} rechargeSoldier Represents the number of ms required to recharge the soldier for an attack.
  * @param {string} name Represents the name of the soldier
  * that is generated as the serial number when the loop goes through an array.
  * @return {Soldier}
  */
  generateSoldier: (rechargeSoldier, name) =>
    new Soldier(rechargeSoldier, 100, name),
};

module.exports = generate;
