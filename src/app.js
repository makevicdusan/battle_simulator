const Strategy = require('./Strategies/Strategies');
const generate = require('./UnitsProduction/Generate');
const Battle = require('./Core/Battle');

/**
* Launches the program.
*/
function run() {
  const armies = generate.generateArmies([
    {
      squads: [5, 7, 7, 5], strategy: Strategy.WEAKEST, rechargeSoldier: 200, rechargeVehicle: 1000,
    },
    {
      squads: [7, 8, 9], strategy: Strategy.STRONGEST, rechargeSoldier: 100, rechargeVehicle: 1200,
    },
    {
      squads: [7, 7, 10], strategy: Strategy.RANDOM, rechargeSoldier: 100, rechargeVehicle: 1100,
    },
  ]);

  const battle = new Battle(armies);
  battle.start();
}
run();
